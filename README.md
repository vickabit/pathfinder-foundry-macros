# Pathfinder Foundry Macros

### Enlarge/Reduce Person Macros

These are a modified version of the "Toggle Buff" Macro built into the PF1E system. Must place a buff called "Enlarge Person" or "Reduce Person", as appropriate for the macro, on the actor for it to work correctly.

Credit to: Adam Bogg for the transform function, obtained (and modified) from his excellent Change Size macro found [here](https://gitlab.com/adambogg/foundry-macros/-/blob/master/Change%20Size.js) and Noon for helping me debug this. I'm clueless in JavaScript, so they were real life-savers.